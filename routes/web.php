<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('landing');
});

Route::get('/artikel', function() {
    return view('artikel');
});

Route::get('/admin/dashboard', 'AdminController@index');
Route::get('/admin/artikel', 'AdminController@artikel');
Route::get('/admin/update-artikel', "AdminController@updateArtikel");
Route::get('/admin/management-users', 'AdminController@managementUser');
Route::get('/admin/add-user', 'AdminController@addUser');
Route::get('/admin/update-users', 'AdminController@updateUsers');
Route::get('/admin/ubah-password', 'AdminController@ubahPassword');
Route::get('/kontributor/dashboard', 'KontributorController@index');
Route::get('/kontributor/artikel', 'KontributorController@artikel');
Route::get('/kontributor/tambah-artikel', 'KontributorController@addArtikel');
Route::get('/kontributor/update-artikel', 'KontributorController@updateArtikel');
Route::get('/kontributor/ubah-password', 'KontributorController@ubahPassword');
Route::get('/users/dashboard', 'UsersController@index');
Route::get('/users/ubah-password', 'UsersController@ubahPassword');
