<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class KontributorController extends Controller
{
    public function index() {
        return view('kontributor.dashboard');
    }

    public function artikel() {
        return view('kontributor.daftarArtikel');
    }

    public function addArtikel() {
        return view('kontributor.formAddArtikel');
    }

    public function updateArtikel() {
        return view('kontributor.formUpdateArtikel');
    }

    public function ubahPassword() {
        return view('kontributor.ubahPassword');
    }
}
