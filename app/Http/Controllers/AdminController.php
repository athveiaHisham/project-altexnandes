<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index() {

        return view('admin.dashboard');
    }

    public function artikel() {
        return view('admin.daftarArtikel');
    }

    public function updateArtikel() {
        return view('admin.formUpdateArtikel');
    }

    public function managementUser() {
        return view('admin.managementUser');
    }

    public function addUser() {
        return view('admin.addUser');
    }

    public function updateUsers() {
        return view('admin.formUpdateUser');
    }

    public function ubahPassword() {
        return view('admin.ubahPassword');
    }
}
