@extends('kontributor.index')

<!-- Isi Judul -->
@section('judul_halaman')
Form Ubah Artikel
@endsection

@section('konten')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">From Ubah Artikel</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/kontributor/dashboard">Home</a></li>
              <li class="breadcrumb-item"><a href="/kontributor/artikel">Daftar Artikel</a></li>
              <li class="breadcrumb-item active">From Ubah Artikel</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

    <!-- Isi Konten disisni -->
    <div class="card">
        <div class="card-body mb-3">
            <form action="" action="">
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="">Judul</label>
                            <input type="text" name="" id="" class="form-control" value="Laravel" required>
                        </div>
                        <div class="form-group">
                            <label for="floatingTextarea2">Isi Artikel</label>
                            <textarea class="form-control" placeholder="Leave a comment here" id="floatingTextarea2" style="height: 300px"
                            >Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet velit quis odio ad, quaerat facilis natus blanditiis non sapiente veritatis modi eveniet rem perferendis tempora id dicta voluptate dolor mollitia aperiam recusandae in officiis placeat incidunt. Placeat eligendi dignissimos ipsa pariatur quis distinctio quae rerum, eaque natus cumque, fugiat blanditiis.</textarea>
                        </div>
                        <div class="form-group">
                            <label for="floatingSelect">Penulis</label>
                            <select class="form-control" id="floatingSelect" aria-label="Floating label select example">
                            <option selected>--Pilih Penulis--</option>
                            <option value="1">Jhnon Doe</option>
                            <option value="2">Two</option>
                            <option value="3">Three</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Tanggal Dibuat</label>
                            <input type="date" class="form-control">
                        </div>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

</div>
@endsection